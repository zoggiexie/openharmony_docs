# 制作img镜像
将uboot与image打包成image

# 脚本内容
```shell
imageName="emmc.img"
ubootDir="../u-boot"
ohosDir="."
# dd 一个200M的空间做img
sudo dd if=/dev/zero of=$imageName  bs=1M count=200
#加载这个镜像通过
devname=$(sudo losetup -f --show emmc.img)
echo $devname
# 写入uboot
sudo dd if="${ubootDir}/u-boot-sunxi-with-spl.bin" of=$imageName conv=notrunc seek=8 bs=1024
# 创建新的bootargs并写入
echo -ne "bootargs=root=emmc fstype=vfat  rootaddr=20M rootsize=20M useraddr=40M usersize=100M"'\0' > bootargs
sudo dd if=bootargs of=$imageName conv=notrunc seek=1024 count=2 bs=1b
#分区 # 第一个分区大小为16M, 存放uImage以及u-boot启动参数
fdisk $devname &>burn.log <<EOF
n
p
1
2048
34815
n
p
2
34816

p
t
1
c
w
EOF
sleep 1
sudo partx -a $devname
sleep 1
sudo mkfs.vfat "${devname}p1"
sudo mkfs.ext4 "${devname}p2"

cat <<EOF > boot_liteos.cmd
load mmc 1:1 0x42000000 uImage
bootm start
bootm loados
bootm go
EOF

"${ubootDir}/tools/mkimage" -T script -A arm -O linux -C none -a0 -e0 -n "boot_liteos_scr" -d boot_liteos.cmd boot.scr
"${ubootDir}/tools/mkimage" -A arm -O linux -T kernel -d "${ohosDir}/OHOS_Image.bin" -C none -a 0x40000000 -e 0x40000020 uImage
sudo mkdir -p sd1
sudo mount "${devname}p1" sd1
sudo cp -v boot.scr uImage sd1
sudo umount sd1
# 注意这里将文件系统直接写进了sdd 20M 和 40M 的位置这是和bootargs里的参数相对应的
# 同时这里也是为了避开sdd1的16M, 直接根据地址计算将文件系统写到了分区2
sudo dd if="${ohosDir}/rootfs_vfat.img" of=$imageName conv=notrunc seek=40960 count=40960 bs=1b

sudo dd if="${ohosDir}/userfs_vfat.img" of=$imageName conv=notrunc seek=81920 count=204800 bs=1b
sleep 1
#卸载
sudo  kpartx -d $devname
sudo losetup -d $devname
```

# 使用方法
修改脚本中头部文件路径变量与生成指定文件
1. 加载uboot
2. 将image通过tftp加载到内存中，写到mmc 0地址
```
#加载uboot
sudo ./sunxi-tools/sunxi-fel uboot ./u-boot/u-boot-sunxi-with-spl.bin
#将image通过tftp加载到内存中，写到mmc 0地址
=> tftp 0x42000000  emmc.img; mmc write 0x42000000  0x0 0x64000 
```