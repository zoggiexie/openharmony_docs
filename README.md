# OpenHarmony Docs

#### 介绍
鸿蒙开发相关文档资料汇总, 包括鸿蒙官方文档, 开发过程中积累的说明文档, 代码仓使用说明, 以及相关的鸿蒙开发教学资料等等.

#### 文件目录
+ docs : 鸿蒙的官方文档仓库, 其中包含整个鸿蒙官方仓库的所有说明性文档, 随官方仓库进行更新.
+ learning :  鸿蒙开发相关的一些学习的资料.

#### 使用说明

1.  该仓库由整个开发团队一起维护, 随着开发进度不断向仓库中补充相关说明性文档.
2.  参与维护的开发人员需联系仓库管理员获取仓库的 push 权限

#### 相关官方仓库链接

1.   **OpenHarmony** : https://gitee.com/openharmony
2.  **OpenHarmony SIG** : https://gitee.com/openharmony-sig
3.  **哈工轩辕** :  https://gitee.com/organizations/hrgxy/projects

